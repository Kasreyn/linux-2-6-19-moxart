/*
 * JFFS2 -- Journalling Flash File System, Version 2.
 *
 * Copyright (C) 2001-2003 Red Hat, Inc.
 *
 * Created by David Woodhouse <dwmw2@infradead.org>
 *
 * For licensing information, see the file 'LICENCE' in this directory.
 *
 * $Id: background.c,v 1.54 2005/05/20 21:37:12 gleixner Exp $
 *
 */

#include <linux/kernel.h>
#include <linux/jffs2.h>
#include <linux/mtd/mtd.h>
#include <linux/completion.h>
#include <linux/sched.h>
#include "nodelist.h"

#if 1 // Add by Jared 02-15-2007. Start the gargage collection.
#include <linux/proc_fs.h>
#include <linux/uaccess.h>
static struct proc_dir_entry *proc_startstop_gc, *proc_jffs2, *proc_dirty, *proc_sector_size;
int jffs2_bstartstop_gc=0; // flag, 1:start the garbage collection, 0:stop the gargabe collection

static int proc_write_startstop_gc(struct file *file, const char *buffer, unsigned long count, void *data)
{
	char			tmpbuf[4];
	int			len;
	struct jffs2_sb_info	*c=(struct jffs2_sb_info *)data;

	if ( count > 4 )
		len = 4;
	else
		len =count;

	if(copy_from_user( tmpbuf, buffer, len)) {
		return -EFAULT;
	}
	tmpbuf[len]='\0';

	sscanf(tmpbuf, "%d", &jffs2_bstartstop_gc);

	if ( jffs2_bstartstop_gc ) // wake up the sleeping garbage collection thread
		wake_up_process(c->gc_task);

	return len;
}

static int proc_read_startstop_gc(char *page, char **start, off_t off, int count, int *eof, void *data)
{
	return sprintf(page, "%d\n", jffs2_bstartstop_gc);
}

static int proc_read_jffs2_dirty(char *page, char **start, off_t off, int count, int *eof, void *data)
{
	struct jffs2_sb_info *c=(struct jffs2_sb_info *)data;

	return sprintf(page, "%d\n", c->dirty_size + c->erasing_size - c->nr_erasing_blocks * c->sector_size);
}

static int proc_read_sector_size(char *page, char **start, off_t off, int count, int *eof, void *data)
{
	struct jffs2_sb_info *c=(struct jffs2_sb_info *)data;

	return sprintf(page, "%d\n", c->nospc_dirty_size);
}
#endif // End: add by Jared

static int jffs2_garbage_collect_thread(void *);

void jffs2_garbage_collect_trigger(struct jffs2_sb_info *c)
{
	spin_lock(&c->erase_completion_lock);
        if (c->gc_task && jffs2_thread_should_wake(c))
                send_sig(SIGHUP, c->gc_task, 1);
	spin_unlock(&c->erase_completion_lock);
}

/* This must only ever be called when no GC thread is currently running */
int jffs2_start_garbage_collect_thread(struct jffs2_sb_info *c)
{
	pid_t pid;
	int ret = 0;

	BUG_ON(c->gc_task);

	init_completion(&c->gc_thread_start);
	init_completion(&c->gc_thread_exit);

	pid = kernel_thread(jffs2_garbage_collect_thread, c, CLONE_FS|CLONE_FILES);
	if (pid < 0) {
		printk(KERN_WARNING "fork failed for JFFS2 garbage collect thread: %d\n", -pid);
		complete(&c->gc_thread_exit);
		ret = pid;
	} else {
		/* Wait for it... */
		D1(printk(KERN_DEBUG "JFFS2: Garbage collect thread is pid %d\n", pid));
		wait_for_completion(&c->gc_thread_start);
	}

	return ret;
}

void jffs2_stop_garbage_collect_thread(struct jffs2_sb_info *c)
{
	int wait = 0;
	spin_lock(&c->erase_completion_lock);
	if (c->gc_task) {
		D1(printk(KERN_DEBUG "jffs2: Killing GC task %d\n", c->gc_task->pid));
		send_sig(SIGKILL, c->gc_task, 1);
		wait = 1;
	}
	spin_unlock(&c->erase_completion_lock);
	if (wait)
		wait_for_completion(&c->gc_thread_exit);
}

static int jffs2_garbage_collect_thread(void *_c)
{
	struct jffs2_sb_info *c = _c;

	daemonize("jffs2_gcd_mtd%d", c->mtd->index);
	allow_signal(SIGKILL);
	allow_signal(SIGSTOP);
	allow_signal(SIGCONT);

	c->gc_task = current;
	complete(&c->gc_thread_start);

#if 1 // Add by Jared 02-15-2007. Start the gargage collection.
	proc_jffs2=proc_mkdir("jffs2", NULL);
	if( proc_jffs2 ==NULL ) {
		printk("<1>Create /proc/jffs2 fail\n");
		return -ENOMEM;
	}

	proc_startstop_gc=create_proc_entry("startstop_gc", 0644, proc_jffs2);
	if( proc_startstop_gc==NULL ) {
		printk("<1>Create /proc/jffs2/startstop_gc fail\n");
		return -ENOMEM;
	}

	proc_startstop_gc->write_proc=proc_write_startstop_gc;
	proc_startstop_gc->read_proc=proc_read_startstop_gc;
	proc_startstop_gc->data=(void*)_c;

	proc_dirty=create_proc_read_entry("dirty", 0444, proc_jffs2, proc_read_jffs2_dirty, (void*)_c);
	if( proc_dirty==NULL ) {
		printk("<1>Create /proc/jffs2/dirty fail\n");
		return -ENOMEM;
	}

	proc_sector_size=create_proc_read_entry("sector_size", 0444, proc_jffs2, proc_read_sector_size, (void*)_c);
	if( proc_sector_size==NULL ) {
		printk("<1>Create /proc/jffs2/sector_size fail\n");
		return -ENOMEM;
	}
#endif // End: add by Jared

	set_user_nice(current, 10);

	for (;;) {
		allow_signal(SIGHUP);

		if (!jffs2_thread_should_wake(c)) {
			set_current_state (TASK_INTERRUPTIBLE);
			D1(printk(KERN_DEBUG "jffs2_garbage_collect_thread sleeping...\n"));
			/* Yes, there's a race here; we checked jffs2_thread_should_wake()
			   before setting current->state to TASK_INTERRUPTIBLE. But it doesn't
			   matter - We don't care if we miss a wakeup, because the GC thread
			   is only an optimisation anyway. */
			schedule();
		}

		if (try_to_freeze())
			continue;

		cond_resched();

		/* Put_super will send a SIGKILL and then wait on the sem.
		 */
		while (signal_pending(current)) {
			siginfo_t info;
			unsigned long signr;

			signr = dequeue_signal_lock(current, &current->blocked, &info);

			switch(signr) {
			case SIGSTOP:
				D1(printk(KERN_DEBUG "jffs2_garbage_collect_thread(): SIGSTOP received.\n"));
				set_current_state(TASK_STOPPED);
				schedule();
				break;

			case SIGKILL:
				D1(printk(KERN_DEBUG "jffs2_garbage_collect_thread(): SIGKILL received.\n"));
				goto die;

			case SIGHUP:
				D1(printk(KERN_DEBUG "jffs2_garbage_collect_thread(): SIGHUP received.\n"));
				break;
			default:
				D1(printk(KERN_DEBUG "jffs2_garbage_collect_thread(): signal %ld received\n", signr));
			}
		}
		/* We don't want SIGHUP to interrupt us. STOP and KILL are OK though. */
		disallow_signal(SIGHUP);

		D1(printk(KERN_DEBUG "jffs2_garbage_collect_thread(): pass\n"));
		if (jffs2_garbage_collect_pass(c) == -ENOSPC) {
			printk(KERN_NOTICE "No space for garbage collection. Aborting GC thread\n");
			goto die;
		}
	}
 die:
	spin_lock(&c->erase_completion_lock);
	c->gc_task = NULL;
	spin_unlock(&c->erase_completion_lock);
	complete_and_exit(&c->gc_thread_exit, 0);
}
