/*
 * pio:		104
 *
 * History:
 * Date		Aurhor			Comment
 * 12-06-2005	Victor Yu.		Create it.
 * 24-05-2006	Jimmy Chen.		Fix it.
 * 02-09-2007	Victor Yu.		Porting to kernel verion 2.6.19.
 */
#if 1	// add by Victor Yu. 02-09-2007
#include <linux/version.h>
#endif
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,12)	// add by Victor Yu. 02-09-2007
#include <linux/config.h>
#endif
#include <asm/arch/moxa.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/miscdevice.h>
#include <linux/fcntl.h>
#include <linux/init.h>
#include <linux/poll.h>
#include <linux/proc_fs.h>
#include <linux/spinlock.h>
#include <linux/delay.h>
#include <linux/rtc.h>
#include <linux/timer.h>
#include <linux/ioport.h>

#include <asm/io.h>
#include <asm/uaccess.h>
#include <asm/system.h>
#include <asm/arch/gpio.h>

#define MOXA_PIO_MINOR		104

#define GPIO_BASE_SHIFT		10
#define GPIO_MASK		(0x3ff<<GPIO_BASE_SHIFT)

//
// PIO file operaiton function call
//
#define MAX_PIO			10
#define PIO_INPUT		1
#define PIO_OUTPUT		0
#define PIO_HIGH		1
#define PIO_LOW			0

#define IOCTL_PIO_GET_MODE      1
#define IOCTL_PIO_SET_MODE      2
#define IOCTL_PIO_GET_DATA      3
#define IOCTL_PIO_SET_DATA      4
struct pio_set_struct {
	int	io_number;
	int	mode_data;	// 1 for input, 0 for output, 1 for high, 0 for low
};

static int pio_ioctl(struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg)
{
	struct pio_set_struct	set;

	switch ( cmd ) {
	case IOCTL_PIO_SET_MODE :
		if ( copy_from_user(&set, (struct pio_set_struct *)arg, sizeof(struct pio_set_struct)) )
			return -EFAULT;
		if ( set.io_number < 0 || set.io_number > MAX_PIO )
			return -EINVAL;
		if ( set.mode_data == PIO_INPUT )
			mcpu_gpio_inout(1<<(set.io_number+GPIO_BASE_SHIFT), MCPU_GPIO_INPUT);
		else if ( set.mode_data == PIO_OUTPUT )
			mcpu_gpio_inout(1<<(set.io_number+GPIO_BASE_SHIFT), MCPU_GPIO_OUTPUT);
		else
			return -EINVAL;
		break;
	case IOCTL_PIO_GET_MODE :
		if ( copy_from_user(&set, (struct pio_set_struct *)arg, sizeof(struct pio_set_struct)) )
			return -EFAULT;
		if ( set.io_number < 0 || set.io_number > MAX_PIO )
			return -EINVAL;
		if ( mcpu_gpio_get_inout(1<<(set.io_number+GPIO_BASE_SHIFT)) == MCPU_GPIO_INPUT )
			set.mode_data = PIO_INPUT;
		else
			set.mode_data = PIO_OUTPUT;
		if ( copy_to_user((struct pio_set_struct *)arg, &set, sizeof(struct pio_set_struct)) )
			return -EFAULT;
		break;
	case IOCTL_PIO_SET_DATA :
		if ( copy_from_user(&set, (struct pio_set_struct *)arg, sizeof(struct pio_set_struct)) )
			return -EFAULT;
		if ( set.io_number < 0 || set.io_number > MAX_PIO )
			return -EINVAL;
		if ( set.mode_data == PIO_HIGH )
			mcpu_gpio_set(1<<(set.io_number+GPIO_BASE_SHIFT), MCPU_GPIO_HIGH);
		else if ( set.mode_data == PIO_LOW )
			mcpu_gpio_set(1<<(set.io_number+GPIO_BASE_SHIFT), MCPU_GPIO_LOW);
		else
			return -EINVAL;
		break;
	case IOCTL_PIO_GET_DATA :
		if ( copy_from_user(&set, (struct pio_set_struct *)arg, sizeof(struct pio_set_struct)) )
			return -EFAULT;
		if ( set.io_number < 0 || set.io_number > MAX_PIO )
			return -EINVAL;
		if ( mcpu_gpio_get(1<<(set.io_number+GPIO_BASE_SHIFT)) )
			set.mode_data = PIO_HIGH;
		else
			set.mode_data = PIO_LOW;
		if ( copy_to_user((struct pio_set_struct *)arg, &set, sizeof(struct pio_set_struct)) )
			return -EFAULT;
		break;
	default:
		return -EINVAL;
	}
	return 0;
}

static int pio_open(struct inode *inode, struct file *file)
{
	if ( MINOR(inode->i_rdev) == MOXA_PIO_MINOR )
		return 0;
	return -ENODEV;
}

static int pio_release(struct inode *inode, struct file *file)
{
	return 0;
}

static struct file_operations pio_fops = {
	owner:THIS_MODULE,
	llseek:NULL,
	ioctl:pio_ioctl,
	open:pio_open,
	release:pio_release,
};
static struct miscdevice pio_dev = {
	MOXA_PIO_MINOR,
	"pio",
	&pio_fops
};

#if 1	// add by Victor Yu. 03-08-2007
extern int	moxa_gpio_sd_used_flag;	// define on arch/arm/kernel/armksyms.c
#endif
static void __exit pio_exit(void)
{
	misc_deregister(&pio_dev);
#if 1	// add by Victor Yu. 12-05-2007
	{
	unsigned long	flags;
	local_irq_save(flags);
	moxa_gpio_sd_used_flag = 0;
	local_irq_restore(flags);
	}
#endif
}

static int __init pio_init(void)
{
	printk("Moxa CPU misc: Register PIO misc ver1.0 ");
#if 1	// add by Victor Yu. 03-08-2007
	{
	unsigned long	flags;
	local_irq_save(flags);
	if ( moxa_gpio_sd_used_flag ) {
		printk("The IO has used by other device driver !\n");
		local_irq_restore(flags);
		return -ENODEV;
	}
	moxa_gpio_sd_used_flag = 1;
	local_irq_restore(flags);
	}
#endif
	if ( misc_register(&pio_dev) ) {
		printk("fail !\n");
		return -ENOMEM;
	}
	
	// set the CPU for GPIO
	mcpu_gpio_mp_set(GPIO_MASK);

	// default set all GPIO for input
	mcpu_gpio_inout(GPIO_MASK, MCPU_GPIO_OUTPUT);

	// default set all GPIO data_out high
	mcpu_gpio_set(GPIO_MASK, MCPU_GPIO_HIGH);
	printk("OK.\n");
	return 0;
}

module_init(pio_init);
module_exit(pio_exit);

MODULE_AUTHOR("Victor Yu");
MODULE_LICENSE("GPL");
