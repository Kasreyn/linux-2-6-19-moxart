/*
 * This is IVTC ODM project to use Moxa CPU for keypad device driver.
 * It is from misc interface. So the device node major number is 10.
 * The device node minor number is following:
 * keypad:	103
 * 
 * There devices are mapping system memory is following:
 * keypad:	0x04004000	write for out, read for in
 *
 * History:
 * Date		Aurhor			Comment
 * 12-05-2005	Victor Yu.		Create it.
 */
#include <linux/config.h>
#include <asm/arch/moxa.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/miscdevice.h>
#include <linux/fcntl.h>
#include <linux/init.h>
#include <linux/poll.h>
#include <linux/proc_fs.h>
#include <linux/spinlock.h>
#include <linux/delay.h>
#include <linux/rtc.h>
#include <linux/timer.h>
#include <linux/ioport.h>

#include <asm/io.h>
#include <asm/uaccess.h>
#include <asm/system.h>

#define KEYPAD_OUT_ADDR		0x84008000
#define KEYPAD_IN_ADDR		0x84008000
#define KEYPAD_RAW_NO		4
#define KEYPAD_COL_NO		6

#define MOXA_KEYPAD_MINOR	103

static spinlock_t		keypad_lock= SPIN_LOCK_UNLOCKED;

//
// keypad file operation function call
//
#define IOCTL_KEYPAD_HAS_PRESS	1
#define IOCTL_KEYPAD_GET_KEY	2
#define IOCTL_KEYPAD_PRESS_TWICE 3

#define KEYPAD_BUFFER_LENGTH	32
#define KEYPAD_BUFFER_MASK	(KEYPAD_BUFFER_LENGTH-1)
#define KEYPAD_POLL_TIME	(HZ/5)

static	int			keypad_wptr=0, keypad_rptr=0;
static 	unsigned int		keypad_buffer[KEYPAD_BUFFER_LENGTH];
static	unsigned int		keypadold;
static	struct timer_list 	keypadtimer;
static	int 			keypadtimer_on=0;
static	int			keypad_opened=0;

static unsigned int	keypad_scan(void)
{
	unsigned int	ret=0, v;
	int		raw, col;


	for ( col=0; col<KEYPAD_COL_NO;) {
		outw((1<<col), KEYPAD_OUT_ADDR);
		col++ ;
		v = inw(KEYPAD_IN_ADDR) & 0x0f;
		ret |= (v << (col*4));
	}
	return ret;
}

static int press_twice = 0 ;
static void keypad_poll(unsigned long ingore)
{
	unsigned int	v, v1;
	
	spin_lock(&keypad_lock);
	del_timer(&keypadtimer);
	keypadtimer_on = 0;
	if ( keypad_opened <= 0 ) {
		spin_unlock(&keypad_lock);
		return;
	}
	v = keypad_scan();
		v1 = v ^ keypadold;
		keypadold = v;
		if ( v && v1 && (keypad_wptr+1) != keypad_rptr ) {
			int index ;
			unsigned char bits = 0 ;

			for(index=0;index<24;index++)
			{
				if(v&(1<<index))
					bits++ ;
				if(bits>1)
				{
					press_twice = 1 ;
					break ;
				}
			}
			keypad_buffer[keypad_wptr++] = v;
			keypad_wptr &= KEYPAD_BUFFER_MASK;

		}

	keypadtimer.function = keypad_poll;
	keypadtimer.expires = jiffies + KEYPAD_POLL_TIME;
	keypadtimer_on = 1;
	add_timer(&keypadtimer);
	spin_unlock(&keypad_lock);
}

static int keypad_ioctl(struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg)
{
	int		v;
	
	switch ( cmd ) {
	case IOCTL_KEYPAD_PRESS_TWICE :
		if ( copy_to_user((void *)arg, &press_twice, sizeof(int)) )
			return -EFAULT;
		break ;
	case IOCTL_KEYPAD_HAS_PRESS :		
		spin_lock(&keypad_lock);
#if 1
		v = (keypad_wptr - keypad_rptr) & KEYPAD_BUFFER_MASK;
#else
		if ( keypad_wptr == keypad_rptr )
			v = 0;
		else 
			v = 1;
#endif
		spin_unlock(&keypad_lock);
		if ( copy_to_user((void *)arg, &v, sizeof(int)) )
			return -EFAULT;
		break;
	case IOCTL_KEYPAD_GET_KEY :
		spin_lock(&keypad_lock);
		if ( keypad_wptr == keypad_rptr )
			v = -1;
		else {
			v = keypad_buffer[keypad_rptr++];
			keypad_rptr &= KEYPAD_BUFFER_MASK;
		}
		spin_unlock(&keypad_lock);
		if ( copy_to_user((void *)arg, &v, sizeof(int)) )
			return -EFAULT;
		press_twice = 0 ;
		break;
	default:
		return -EINVAL;
	}
	return 0;
}

static int keypad_open(struct inode *inode, struct file *file)
{
	if ( MINOR(inode->i_rdev) != MOXA_KEYPAD_MINOR )
		return -ENODEV;
	spin_lock(&keypad_lock);
	if ( keypad_opened == 0 ) {
		keypad_wptr = keypad_rptr = 0;
		keypadold = keypad_scan();
		keypadtimer.function = keypad_poll;
		keypadtimer.expires = jiffies + KEYPAD_POLL_TIME;
		keypadtimer_on = 1;
		add_timer(&keypadtimer);
	}
	keypad_opened++;
	spin_unlock(&keypad_lock);
	return 0;
}

static int keypad_release(struct inode *inode, struct file *file)
{
	spin_lock(&keypad_lock);
	if ( keypad_opened > 0 ) {
		keypad_opened--;
		if ( keypad_opened == 0 ) {
			if ( keypadtimer_on ) {
				del_timer(&keypadtimer);
				keypadtimer_on = 0;
			}
			keypad_wptr = keypad_rptr = 0;
		}
	}
	spin_unlock(&keypad_lock);
	return 0;
}

static struct file_operations keypad_fops = {
	owner:THIS_MODULE,
	llseek:NULL,
	ioctl:keypad_ioctl,
	open:keypad_open,
	release:keypad_release,
};
static struct miscdevice keypad_dev = {
	MOXA_KEYPAD_MINOR,
	"keypad",
	&keypad_fops
};

static void __exit ivtc_keypad_exit(void)
{
	spin_lock(&keypad_lock);
	if ( keypadtimer_on ) {
		del_timer(&keypadtimer);
		keypadtimer_on = 0;
	}
	spin_unlock(&keypad_lock);
	misc_deregister(&keypad_dev);
}

static int __init ivtc_keypad_init(void)
{
	printk("Moxa CPU misc: Register Keypad misc ver1.0 ");
	if ( misc_register(&keypad_dev) ) {
		printk("fail !\n");
		return -ENOMEM;
	}
	init_timer(&keypadtimer);
	printk("OK.\n");

	return 0;
}

module_init(ivtc_keypad_init);
module_exit(ivtc_keypad_exit);

MODULE_AUTHOR("Victor Yu");
MODULE_LICENSE("GPL");
