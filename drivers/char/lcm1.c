/*
 * Copyright (C) MOXA Inc. All rights reserved.
 *
 * This software is distributed under the terms of the
 * MOXA License.  See the file COPYING-MOXA for details.
 *
 * This is Moxa CPU for IVTC ODM LCM moudle WG240128B device driver.
 * It is from misc interface. So the device node major number is 10.
 * The device node minor number is following:
 * lcm	:	102
 * 
 * There devices are mapping system memory is following:
 * lcm	:	0x04000000	read only, data -> LCM & control signal
 *	:	0x04002000	write only, LCM -> data & control signal
 *
 * History:
 * Date		Aurhor			Comment
 * 12-05-2005	Victor Yu.		Create it.
 * 01-19-2005	Jimmy Chen.		Fix to work.
 * 09-03-2008	Victor Yu.		Modify to support IVTC write byte feature.
 *
 * There devices are mapping system memory is following:
 * lcm	:	0x84004000	read only, data -> LCM & control signal
 *	:	0x84000000	write only, LCM -> data & control signal
 *
 */
#include <linux/config.h>
#include <asm/arch/moxa.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/miscdevice.h>
#include <linux/fcntl.h>
#include <linux/init.h>
#include <linux/poll.h>
#include <linux/proc_fs.h>
#include <linux/spinlock.h>
#include <linux/delay.h>
#include <linux/rtc.h>
#include <linux/timer.h>
#include <linux/ioport.h>

#include <asm/io.h>
#include <asm/uaccess.h>
#include <asm/system.h>
#include "moxalcm.h"

#define LCM_WRITE_ADDR		0x84000000
#define LCM_READ_ADDR		0x84004000

#define MOXA_LCM_MINOR		102

static spinlock_t		lcm_lock= SPIN_LOCK_UNLOCKED;

//
// LCM file operation function call
//
static unsigned char fnt8x8[]={
0x70,0x88,0x98,0xA8,0xC8,0x88,0x70,0x00,0x20,0x60,0x20,0x20,0x20,0x20,0xF8,0x00,
0x70,0x88,0x08,0x10,0x20,0x48,0xF8,0x00,0x70,0x88,0x08,0x70,0x08,0x88,0x70,0x00,
0x10,0x30,0x50,0x90,0xF8,0x10,0x38,0x00,0xF8,0x80,0xF0,0x08,0x08,0x88,0x70,0x00,
0x30,0x48,0x80,0xF0,0x88,0x88,0x70,0x00,0xF8,0x88,0x10,0x20,0x20,0x20,0x20,0x00,
0x70,0x88,0x88,0x70,0x88,0x88,0x70,0x00,0x70,0x88,0x88,0x78,0x08,0x88,0x70,0x00,
0x20,0x50,0x88,0x88,0xF8,0x88,0x88,0x00,0xF0,0x48,0x48,0x70,0x48,0x48,0xF0,0x00,
0x30,0x48,0x80,0x80,0x80,0x48,0x30,0x00,0xE0,0x90,0x88,0x88,0x88,0x90,0xE0,0x00,
0xF8,0x48,0x40,0x70,0x40,0x48,0xF8,0x00,0xF8,0x48,0x40,0x70,0x40,0x40,0xE0,0x00,
0x80,0xE0,0xF8,0xFE,0xF8,0xE0,0x80,0x00,0x02,0x0E,0x3E,0xFE,0x3E,0x0E,0x02,0x00,
0x18,0x3C,0x7E,0x18,0x18,0x7E,0x3C,0x18,0x66,0x66,0x66,0x66,0x66,0x00,0x66,0x00,
0x7F,0xDB,0xDB,0x7B,0x1B,0x1B,0x1B,0x00,0x3E,0x63,0x38,0x6C,0x6C,0x38,0xCC,0x78,
0x00,0x00,0x00,0x00,0x7E,0x7E,0x7E,0x00,0x18,0x3C,0x7E,0x18,0x7E,0x3C,0x18,0xFF,
0x18,0x3C,0x7E,0x18,0x18,0x18,0x18,0x00,0x18,0x18,0x18,0x18,0x7E,0x3C,0x18,0x00,
0x00,0x18,0x0C,0xFE,0x0C,0x18,0x00,0x00,0x00,0x30,0x60,0xFE,0x60,0x30,0x00,0x00,
0x00,0x00,0xC0,0xC0,0xC0,0xFE,0x00,0x00,0x00,0x24,0x66,0xFF,0x66,0x24,0x00,0x00,
0x00,0x18,0x3C,0x7E,0xFF,0xFF,0x00,0x00,0x00,0xFF,0xFF,0x7E,0x3C,0x18,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x10,0x10,0x10,0x10,0x10,0x00,0x10,0x00,
0x44,0x44,0x44,0x00,0x00,0x00,0x00,0x00,0x44,0x44,0xFE,0x44,0xFE,0x44,0x44,0x00,
0x20,0x70,0x80,0x70,0x08,0xF0,0x20,0x00,0xC2,0xC4,0x08,0x10,0x20,0x46,0x86,0x00,
0x70,0x88,0x50,0x74,0x88,0x84,0x7A,0x00,0x40,0x40,0x80,0x00,0x00,0x00,0x00,0x00,
0x08,0x10,0x20,0x20,0x20,0x10,0x08,0x00,0x20,0x10,0x08,0x08,0x08,0x10,0x20,0x00,
0x00,0x42,0x24,0xFF,0x24,0x42,0x00,0x00,0x00,0x10,0x10,0x7C,0x10,0x10,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x10,0x10,0x20,0x00,0x00,0x00,0xF8,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x30,0x30,0x00,0x02,0x04,0x08,0x10,0x20,0x40,0x80,0x00,
0x7C,0x86,0x8A,0x92,0xA2,0xC2,0x7C,0x00,0x20,0x60,0x20,0x20,0x20,0x20,0xF8,0x00,
0x78,0x84,0x04,0x18,0x60,0x80,0xFC,0x00,0x78,0x84,0x04,0x18,0x04,0x84,0x78,0x00,
0x1C,0x24,0x44,0x84,0xFE,0x04,0x0E,0x00,0xFC,0x80,0xF8,0x04,0x04,0x84,0x78,0x00,
0x30,0x40,0x80,0xF8,0x84,0x84,0x78,0x00,0xFC,0x84,0x04,0x08,0x10,0x10,0x10,0x00,
0x78,0x84,0x84,0x78,0x84,0x84,0x78,0x00,0x78,0x84,0x84,0x7C,0x04,0x08,0x70,0x00,
0x00,0x20,0x20,0x00,0x00,0x20,0x20,0x00,0x00,0x20,0x20,0x00,0x00,0x20,0x20,0x40,
0x10,0x20,0x40,0x80,0x40,0x20,0x10,0x00,0x00,0x00,0xF8,0x00,0x00,0xF8,0x00,0x00,
0x20,0x10,0x08,0x04,0x08,0x10,0x20,0x00,0x78,0x84,0x04,0x08,0x10,0x00,0x10,0x00,
0x7C,0x82,0x82,0x82,0x9C,0x80,0x78,0x00,0x20,0x50,0x88,0x88,0xF8,0x88,0x88,0x00,
0xF8,0x44,0x44,0x78,0x44,0x44,0xF8,0x00,0x38,0x44,0x80,0x80,0x80,0x44,0x38,0x00,
0xF8,0x44,0x42,0x42,0x42,0x44,0xF8,0x00,0xFE,0x80,0x80,0xF0,0x80,0x80,0xFE,0x00,
0xFE,0x80,0x80,0xF0,0x80,0x80,0x80,0x00,0x3C,0x42,0x80,0x80,0x86,0x42,0x3C,0x00,
0x84,0x84,0x84,0xFC,0x84,0x84,0x84,0x00,0x70,0x20,0x20,0x20,0x20,0x20,0x70,0x00,
0x0E,0x04,0x04,0x04,0x84,0x84,0x78,0x00,0x42,0x42,0x44,0x78,0x44,0x42,0x42,0x00,
0x40,0x40,0x40,0x40,0x40,0x40,0x7E,0x00,0x82,0xC6,0xAA,0x92,0x82,0x82,0x82,0x00,
0x82,0xC2,0xA2,0x92,0x8A,0x86,0x82,0x00,0x38,0x44,0x82,0x82,0x82,0x44,0x38,0x00,
0x7C,0x42,0x42,0x7C,0x40,0x40,0x40,0x00,0x78,0x84,0x84,0x84,0x84,0x78,0x0C,0x00,
0xF8,0x84,0x84,0xF8,0x88,0x84,0x84,0x00,0x78,0x84,0x80,0x78,0x04,0x84,0x78,0x00,
0xFE,0x10,0x10,0x10,0x10,0x10,0x10,0x00,0x84,0x84,0x84,0x84,0x84,0x84,0x78,0x00,
0x84,0x84,0x84,0x84,0x84,0x48,0x30,0x00,0x82,0x82,0x82,0x92,0xAA,0xC6,0x82,0x00,
0x82,0x44,0x28,0x10,0x28,0x44,0x82,0x00,0x88,0x88,0x88,0x70,0x20,0x20,0x20,0x00,
0xFE,0x04,0x08,0x10,0x20,0x40,0xFE,0x00,0x78,0x40,0x40,0x40,0x40,0x40,0x78,0x00,
0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x00,0x78,0x08,0x08,0x08,0x08,0x08,0x78,0x00,
0x10,0x28,0x44,0x82,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,
0x10,0x10,0x08,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x78,0x04,0x7C,0x84,0x76,0x00,
0x40,0x40,0x40,0x7C,0x42,0x42,0x7C,0x00,0x00,0x00,0x78,0x84,0x80,0x84,0x78,0x00,
0x04,0x04,0x04,0x7C,0x84,0x84,0x78,0x00,0x00,0x00,0x78,0x84,0xF8,0x80,0x78,0x00,
0x38,0x44,0x40,0xF0,0x40,0x40,0x40,0x00,0x00,0x00,0x78,0x84,0x84,0x7C,0x04,0xF8,
0x40,0x40,0x40,0x7C,0x42,0x42,0x42,0x00,0x10,0x00,0x10,0x10,0x10,0x10,0x10,0x00,
0x04,0x00,0x04,0x04,0x04,0x84,0x84,0x78,0x80,0x80,0x84,0x88,0xF0,0x88,0x84,0x00,
0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x00,0x00,0x00,0x44,0xAA,0x92,0x82,0x82,0x00,
0x00,0x00,0xF8,0x84,0x84,0x84,0x84,0x00,0x00,0x00,0x78,0x84,0x84,0x84,0x78,0x00,
0x00,0x00,0xF8,0x84,0x84,0xF8,0x80,0x80,0x00,0x00,0x7C,0x84,0x84,0x7C,0x04,0x04,
0x00,0x00,0xB8,0xC4,0x80,0x80,0x80,0x00,0x00,0x00,0x7C,0x80,0x78,0x04,0xF8,0x00,
0x10,0x10,0x38,0x10,0x10,0x10,0x10,0x00,0x00,0x00,0x84,0x84,0x84,0x84,0x78,0x00,
0x00,0x00,0x84,0x84,0x84,0x48,0x30,0x00,0x00,0x00,0x82,0x82,0x92,0xAA,0x44,0x00,
0x00,0x00,0x44,0x28,0x10,0x28,0x44,0x00,0x00,0x00,0x84,0x84,0x84,0x7C,0x04,0xF8,
0x00,0x00,0xFC,0x08,0x10,0x20,0xFC,0x00,0x0C,0x10,0x10,0x60,0x10,0x10,0x0C,0x00,
0x10,0x10,0x10,0x00,0x10,0x10,0x10,0x00,0xC0,0x20,0x20,0x18,0x20,0x20,0xC0,0x00,
0x76,0xDC,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x10,0x28,0x44,0x82,0x82,0xFE,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
};
static int		lcmx, lcmy;	// dots position

// following on LCM command
#define LCM_CMD_SET_CUR_POINT		0x21	// set cursor pointer
#define LCM_CMD_SET_OFF_REG		0x22	// set offset register
#define LCM_CMD_SET_ADDR_POINT		0x24	// set address pointer
#define LCM_CMD_SET_T_H_ADDR		0x40	// set text home address
#define LCM_CMD_SET_T_AREA		0x41	// set text area
#define LCM_CMD_SET_G_H_ADDR		0x42	// set graphic home address
#define LCM_CMD_SET_G_AREA		0x43	// set graphic area
#define LCM_CMD_OR_MODE			0x80	// OR mode
#define LCM_CMD_EXOR_MODE		0x81	// EXOR mode
#define LCM_CMD_AND_MODE		0x83	// AND mode
#define LCM_CMD_T_ATTR			0x84	// text attribute mode
#define LCM_CMD_INT_CG_ROM		0x85	// internal CG ROM mode
#define LCM_CMD_EXT_CG_RAM		0x88	// external CG RAM mode
#define LCM_CMD_DISPLAY_OFF		0x90	// display off
#define LCM_CMD_CO_BF			0x92	// cursor on, blink off
#define LCM_CMD_CO_BO			0x93	// cursor on, blink on
#define LCM_CMD_TO_GF			0x94	// text on, graphic off
#define LCM_CMD_TF_GO			0x98	// text off, graphic on
#define LCM_CMD_TO_GO			0x9c	// text on, graphic on
#define LCM_CMD_CUR_1			0xa0	// 1-line cursor
#define LCM_CMD_CUR_2			0xa1	// 2-line cursor
#define LCM_CMD_CUR_3			0xa2	// 3-line cursor
#define LCM_CMD_CUR_4			0xa3	// 4-line cursor
#define LCM_CMD_CUR_5			0xa4	// 5-line cursor
#define LCM_CMD_CUR_6			0xa5	// 6-line cursor
#define LCM_CMD_CUR_7			0xa6	// 7-line cursor
#define LCM_CMD_CUR_8			0xa7	// 8-line cursor
#define LCM_CMD_DATA_AW			0xb0	// set data auto write
#define LCM_CMD_DATA_AR			0xb1	// set data auto read
#define LCM_CMD_AUTO_RESET		0xb2	// auto reset
#define LCM_CMD_DW_INC			0xc0	// data write and increment ADP
#define LCM_CMD_DR_INC			0xc1	// data read and increment ADP
#define LCM_CMD_DW_DEC			0xc2	// data write and decrement ADP
#define LCM_CMD_DR_DEC			0xc3	// data read and decrement ADP
#define LCM_CMD_DW_NON			0xc4	// data write and nonvariable ADP
#define LCM_CMD_DR_NON			0xc5	// data read and nonvariable ADP
#define LCM_CMD_SCR_PEEk		0xe0	// screen peek
#define LCM_CMD_SCR_COPY		0xe8	// screen copy
#define LCM_CMD_BIT_RESET		0xf0	// bit reset
#define LCM_CMD_BIT_SET			0xf9	// bit set
#define LCM_CMD_BIT_0			0xf8	// bit 0
#define LCM_CMD_BIT_1			0xf9	// bit 1
#define LCM_CMD_BIT_2			0xfa	// bit 2
#define LCM_CMD_BIT_3			0xfb	// bit 3
#define LCM_CMD_BIT_4			0xfc	// bit 4
#define LCM_CMD_BIT_5			0xfd	// bit 5
#define LCM_CMD_BIT_6			0xfe	// bit 6
#define LCM_CMD_BIT_7			0xff	// bit 7

// following status
#define LCM_CMD_EXEC_CAP		(1<<0)	// check command execution cap.
#define LCM_DATA_RW_CAP			(1<<1)	// check data read/write cap.
#define LCM_AUTO_RD_CAP			(1<<2)	// check auto mode data read cap.
#define LCM_AUTO_WD_CAP			(1<<3)	// check auto mode data write cap.
#define LCM_CTRL_OP_CAP			(1<<5)	// check controller operation cap.
#define LCM_ERR_FLAG			(1<<6)	// error flag
#define LCM_BLINK_CON			(1<<7)	// check the blink condition

// following LCM control for CPU
#define CD_0			0
#define CD_1			(1<<8)	// when write
#define CE_1			(1<<10)
#define CE_0			0
#define RD_1			(1<<9)	// data write
#define RD_0			0
#define WR_1			(1<<12)	// data read
#define WR_0			0
#define LCM_STATUS_READ		(1<<8)	// when read
#define LCM_READ_ENABLE		(1<<14)
#define LCM_WRITE_ENABLE	0
#define LCM_DATA_MASK		0xff

//DATA AUTO READ/WRITE
#define	LCM_DATA_ATOW	0xB0	//Set Data Auto Write
#define	LCM_DATA_ATOR	0xB1	//Set Data Auto Read
#define	LCM_AUTO_RESET	0xB2	//Auto Reset

#define LCM_MAX_X_DOTS		240
#define LCM_MAX_Y_DOTS		128
#define LCM_HALF_X_DOTS		(LCM_MAX_X_DOTS/2)
#define LCM_X_DOTS		8
#define LCM_Y_DOTS		8
#define LCM_MAX_X		(LCM_MAX_X_DOTS/LCM_X_DOTS)
#define LCM_MAX_Y		(LCM_MAX_Y_DOTS/LCM_Y_DOTS)
#define LCM_ADDRESS_X_Y(x,y)	((y)*LCM_MAX_X + x)

static unsigned char	lcm_pix_buffer[LCM_MAX_X_DOTS][LCM_MAX_Y_DOTS];
static int		lcm_auto_scroll_flag=1;
static int		lcm_reverse_flag=0;
typedef struct lcm_xy {
	int	x;	// 0 - 15
	int	y;	// 0 - 7
} lcm_xy_t;

#define LCM_NOT_LOCK_INT
static inline int	LCM_check_status(u16 check_status)
{
#define LCM_RETRY_COUNT		10
	int		i, ret = 0;
	u16 status ;

	for ( i=0; i<LCM_RETRY_COUNT; i++ ) {
		outw(CD_0|CE_0|RD_1|WR_1|LCM_READ_ENABLE,LCM_WRITE_ADDR) ;
		outw(CD_1|CE_0|RD_0|WR_1|LCM_READ_ENABLE, LCM_WRITE_ADDR);
		udelay(1) ;
		status = inw(LCM_READ_ADDR) ;
		if ( status & check_status ) {
			ret = 1;
			break;
		}
	}
	/* back to initial */
	outw(CD_0|CE_1|RD_1|WR_1|LCM_READ_ENABLE,LCM_WRITE_ADDR) ;
	return ret;
}

static inline void	LCM_write_cmd(u16 cmd)
{
	outw(CD_0|CE_1|RD_1|WR_1|LCM_READ_ENABLE,LCM_WRITE_ADDR) ;
	udelay(1) ;
	outw(CD_1|CE_1|RD_1|WR_1|LCM_WRITE_ENABLE,LCM_WRITE_ADDR) ;
	udelay(1) ;
	outw(CD_1|CE_0|RD_1|WR_0|LCM_WRITE_ENABLE,LCM_WRITE_ADDR) ;
	udelay(1) ;
	outw(CD_1|CE_0|RD_1|WR_0|LCM_WRITE_ENABLE|cmd,LCM_WRITE_ADDR) ;
	udelay(1) ;
	outw(CD_1|CE_0|RD_1|WR_1|LCM_WRITE_ENABLE|cmd,LCM_WRITE_ADDR) ;
	udelay(1) ;
	/*back to initial*/
	outw(CD_0|CE_1|RD_1|WR_1|LCM_READ_ENABLE|cmd,LCM_WRITE_ADDR) ;
}

static inline void	LCM_write_data(u16 data)
{
	outw(CD_1|CE_1|RD_1|WR_1|LCM_READ_ENABLE,LCM_WRITE_ADDR) ;
	udelay(1) ;
	outw(CD_0|CE_1|RD_1|WR_1|LCM_WRITE_ENABLE,LCM_WRITE_ADDR) ;
	udelay(1) ;
	outw(CD_0|CE_0|RD_1|WR_0|LCM_WRITE_ENABLE,LCM_WRITE_ADDR) ;
	udelay(1) ;
	outw(CD_0|CE_0|RD_1|WR_0|LCM_WRITE_ENABLE|data,LCM_WRITE_ADDR) ;
	udelay(1) ;
	outw(CD_0|CE_0|RD_1|WR_1|LCM_WRITE_ENABLE|data,LCM_WRITE_ADDR) ;
	udelay(1) ;
	/*back to initial*/
	outw(CD_1|CE_1|RD_1|WR_1|LCM_READ_ENABLE|data,LCM_WRITE_ADDR) ;
}

static void	LCM_no_argument_cmd(u16 cmd)
{
	spin_lock(&lcm_lock);
	if ( LCM_check_status(LCM_CMD_EXEC_CAP) )
		LCM_write_cmd(cmd);
	spin_unlock(&lcm_lock);
}

static void 	LCM_one_argument_cmd(u16 cmd, u16 data)
{
	spin_lock(&lcm_lock);
	if ( !LCM_check_status(LCM_CMD_EXEC_CAP) )
		goto one_cmd_end;
	LCM_write_data(data);
	if ( !LCM_check_status(LCM_CMD_EXEC_CAP) )
		goto one_cmd_end;
	LCM_write_cmd(cmd);
one_cmd_end:
	spin_unlock(&lcm_lock);
}

static void	LCM_two_arguments_cmd(u16 cmd, u16 data1, u16 data2)
{
	spin_lock(&lcm_lock);
	if ( !LCM_check_status(LCM_CMD_EXEC_CAP) )
		goto two_cmd_end;
	LCM_write_data(data1);
	if ( !LCM_check_status(LCM_CMD_EXEC_CAP) )
		goto two_cmd_end;
	LCM_write_data(data2);
	if ( !LCM_check_status(LCM_CMD_EXEC_CAP) )
		goto two_cmd_end;
	LCM_write_cmd(cmd);
two_cmd_end:
	spin_unlock(&lcm_lock);
}

static void lcm_up_one_line(void)
{
	int	x, y, index, i, addr;

	// first up one line for lcm buffer
	spin_lock(&lcm_lock);
	for ( y=1; y<LCM_MAX_Y; y++ ) {
		for ( x=0; x<LCM_MAX_X; x++ ) {
			memcpy(&lcm_pix_buffer[y-1][x*LCM_X_DOTS], &lcm_pix_buffer[y][x*LCM_X_DOTS], LCM_X_DOTS);
		}
	}
	for ( x=0; x<LCM_MAX_X; x++ ) {
		memset(&lcm_pix_buffer[LCM_MAX_Y-1][x*LCM_X_DOTS], 0, LCM_X_DOTS);
	}
	spin_unlock(&lcm_lock);

	// second redisplay up one line on LCM
	LCM_no_argument_cmd(LCM_CMD_DISPLAY_OFF);
	for ( y=0; y<LCM_MAX_Y; y++ ) {
		for ( x=0; x<LCM_MAX_X; x++ ) {
			addr = LCM_ADDRESS_X_Y(x, y);
			LCM_two_arguments_cmd(LCM_CMD_SET_ADDR_POINT, addr&0xff, (addr>>8)&0xff);
			index = x * LCM_X_DOTS;
			for ( i=0; i<LCM_X_DOTS; i++, index++ ) {
				LCM_one_argument_cmd(LCM_CMD_DW_INC, (u16)lcm_pix_buffer[y][index]);
			}
		}
	}
	LCM_no_argument_cmd(LCM_CMD_TF_GO);
	spin_lock(&lcm_lock);
	lcmx = 0;
	lcmy = LCM_MAX_Y - 1;
	spin_unlock(&lcm_lock);
}

static void LCM_printf_char(unsigned char ch)
{
	int index, i ;
	u16 addr;
	
	if ( ch == '\n' ) {
		spin_lock(&lcm_lock);
		for ( i=(lcmx/LCM_X_DOTS); i<LCM_MAX_X; i++ ) {
			if ( lcm_reverse_flag )
				memset(&lcm_pix_buffer[lcmy][i*LCM_X_DOTS], 1, LCM_X_DOTS);
			else
				memset(&lcm_pix_buffer[lcmy][i*LCM_X_DOTS], 0, LCM_X_DOTS);
		}
		lcmx = 0;
		lcmy++;
		if ( lcmy >= LCM_MAX_Y ) {
			if ( lcm_auto_scroll_flag ) {
				lcmy = LCM_MAX_Y - 1;
				spin_unlock(&lcm_lock);
				lcm_up_one_line();
				return;
			} else {
				lcmy = 0;
			}
		}
		spin_unlock(&lcm_lock);
		return;
	}
	addr = ((lcmy*8)*0x1e) + (lcmx/LCM_X_DOTS) ;
	//addr = LCM_ADDRESS_X_Y(lcmx, lcmy);
	LCM_two_arguments_cmd(LCM_CMD_SET_ADDR_POINT,(u16)addr&0xFF,(u16)(addr>>8));
	
	index = ch * LCM_X_DOTS;
	for ( i=0; i<LCM_Y_DOTS; i++, index++, lcmx++ ) {
		spin_lock(&lcm_lock);
		if ( lcm_reverse_flag ) 
			lcm_pix_buffer[lcmy][lcmx] = ~fnt8x8[index];
		else
			lcm_pix_buffer[lcmy][lcmx] = fnt8x8[index];
		spin_unlock(&lcm_lock);
		//LCM_one_argument_cmd(LCM_CMD_DW_INC, (u16)fnt8x8[index]);
		
		LCM_two_arguments_cmd(LCM_CMD_SET_ADDR_POINT,\
				(u16)(addr+(0x1e*i))&0xFF,(u16)(addr+(0x1e*i))>>8) ;
		LCM_one_argument_cmd(LCM_CMD_DW_INC,(u16)fnt8x8[index]);
	}
	spin_lock(&lcm_lock);
	if ( lcmx >= LCM_MAX_X_DOTS ) {
		lcmx = 0;
		lcmy++;
		if ( lcmy >= LCM_MAX_Y ) {
			if ( lcm_auto_scroll_flag ) {
				lcmy = LCM_MAX_Y - 1;
				spin_unlock(&lcm_lock);
				lcm_up_one_line();
				return;
			} else {
				lcmy = 0;
			}
		}
	}
	spin_unlock(&lcm_lock);
}

static void LCM_clear_line(void)
{
	int 	x, index, i, addr;

	for ( x=0; x<LCM_MAX_X; x++ ) {
		spin_lock(&lcm_lock);
		if ( lcm_reverse_flag )
			memset(&lcm_pix_buffer[lcmy][x*LCM_X_DOTS], 1, LCM_X_DOTS);
		else
			memset(&lcm_pix_buffer[lcmy][x*LCM_X_DOTS], 0, LCM_X_DOTS);
		spin_unlock(&lcm_lock);
		addr = LCM_ADDRESS_X_Y(x, lcmy);
		LCM_two_arguments_cmd(LCM_CMD_SET_ADDR_POINT, addr&0xff, (addr>>8)&0xff);
		index = x * LCM_X_DOTS;
		for ( i=0; i<LCM_X_DOTS; i++, index++ ) {
			LCM_one_argument_cmd(LCM_CMD_DW_INC, (u16)lcm_pix_buffer[lcmy][index]);
		}
	}
	spin_lock(&lcm_lock);
	lcmx = 0;
	spin_unlock(&lcm_lock);
}
static void LCM_clear(void)
{
	int i , x , y ;
	i = x = y = 0 ;
	LCM_two_arguments_cmd(LCM_CMD_SET_ADDR_POINT,0,0) ;
	//LCM_no_argument_cmd(LCM_DATA_ATOW) ;
	for(;i<(LCM_MAX_X*LCM_MAX_Y_DOTS);i++)
		LCM_one_argument_cmd(LCM_CMD_DW_INC,0x00);
	//LCM_no_argument_cmd(LCM_AUTO_RESET) ;
	
	memset(&lcm_pix_buffer,0,(LCM_MAX_X_DOTS*LCM_MAX_Y_DOTS)) ;
	lcmx = lcmy = 0 ;
}

#if 0	// mask by Victor Yu. 09-03-2008, not used
static void LCM_write_string(u16 column,char *string,int size)
{
	LCM_two_arguments_cmd(LCM_CMD_SET_ADDR_POINT,column&0xFF,column>>8) ;
	//LCM_no_argument_cmd(LCM_DATA_ATOW) ;
	while(size--)
		LCM_one_argument_cmd(LCM_CMD_DW_INC,(*string++)-' ');
	//LCM_no_argument_cmd(LCM_AUTO_RESET) ;
}
static void LCM_write_graph(u16 column,u16 data)
{
	LCM_two_arguments_cmd(LCM_CMD_SET_ADDR_POINT,column&0xFF,column>>8) ;
	//LCM_no_argument_cmd(LCM_DATA_ATOW) ;
	LCM_one_argument_cmd(LCM_CMD_DW_INC,data);
	//LCM_no_argument_cmd(LCM_AUTO_RESET) ;
}
#endif

#define T_AREA_SIZE 0x1e
#define G_AREA_SIZE 0x1e
#define TEST_STRING0 "Hello world !! Phone: 02-89191"
#define TEST_STRING1 "230 # 235"

/*
 * I don't know why cannot malloc memory from kernel. 
 * 03-10-2004	Victor Yu.
 */
#define USE_WRITE_BUFFER
static ssize_t lcm_write(struct file * filp, const char * buf, size_t count, loff_t *ppos)
{
#ifdef USE_WRITE_BUFFER
	char	write_buffer[LCM_MAX_X * LCM_MAX_Y];
#else
	char	*ptr;
#endif
	int	i;
	
	if ( count == 0 )
		return 0;
#ifdef USE_WRITE_BUFFER
	if ( count > (LCM_MAX_X * LCM_MAX_Y) )
		count = LCM_MAX_X * LCM_MAX_Y;
	if ( copy_from_user(write_buffer, buf, count) ) {
		return -EFAULT;
	}
	for ( i=0; i<count; i++ )
		LCM_printf_char(write_buffer[i]);
#else
	ptr = kmalloc(count, GFP_KERNEL);
	if ( ptr == NULL )
		return -ENOMEM;
	if ( copy_from_user(ptr, buf, count) ) {
		kfree(ptr);
		return -EFAULT;
	}
	for ( i=0; i<count; i++ )
		LCM_printf_char(*ptr++);
	kfree(ptr);
#endif
	return count;
}

static int lcm_ioctl(struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg)	  
{
	lcm_xy_t		pos;
	static unsigned char	user_data;

	switch ( cmd ) {
	case IOCTL_LCM_PIX_ON :
	case IOCTL_LCM_PIX_OFF :
	{
		unsigned char	v, mask;
		u16		addr;

		if ( copy_from_user(&pos, (void *)arg, sizeof(pos)) )
			return -EFAULT;
		if ( pos.x < 0 || pos.x >= LCM_MAX_X_DOTS || pos.y < 0 || pos.y >= LCM_MAX_Y_DOTS )
			return -EINVAL;
		spin_lock(&lcm_lock);
		v = lcm_pix_buffer[pos.y][pos.x/LCM_X_DOTS];
		mask = 0x80 >> (pos.x % 8);
		if ( cmd == IOCTL_LCM_PIX_ON )
			v |= mask;
		else
			v &= ~mask;
		lcm_pix_buffer[pos.y][pos.x/LCM_X_DOTS] = v;
		spin_unlock(&lcm_lock);
		addr = ((pos.y)*0x1e) + (pos.x/LCM_X_DOTS) ;
		LCM_two_arguments_cmd(LCM_CMD_SET_ADDR_POINT,(u16)(addr&0xFF),(u16)(addr>>8));
		LCM_one_argument_cmd(LCM_CMD_DW_INC,(u16)v);
		break;
	}
	case IOCTL_LCM_GOTO_XY :
		if ( copy_from_user(&pos, (void *)arg, sizeof(pos)) )
			return -EFAULT;
		if ( pos.x < 0 || pos.x >= LCM_MAX_X || pos.y < 0 || pos.y >= LCM_MAX_Y )
			return -EINVAL;
		spin_lock(&lcm_lock);
		lcmx = pos.x * LCM_X_DOTS;
		lcmy = pos.y;
		spin_unlock(&lcm_lock);
		break;
	case IOCTL_LCM_CLS :
		LCM_clear();
		break;
	case IOCTL_LCM_CLEAN_LINE :
		LCM_clear_line();
		break;
	case IOCTL_LCM_GET_XY :
		spin_lock(&lcm_lock);
		pos.x = lcmx / LCM_X_DOTS;
		pos.y = lcmy;
		spin_unlock(&lcm_lock);
		if ( copy_to_user((void *)arg, &pos,sizeof(pos)) )
			return -EFAULT;
		break;
	case IOCTL_LCM_BACK_LIGHT_ON :
	case IOCTL_LCM_BACK_LIGHT_OFF :
		break;
	case IOCTL_LCM_AUTO_SCROLL_ON :
		spin_lock(&lcm_lock);
		lcm_auto_scroll_flag = 1;
		spin_unlock(&lcm_lock);
		break;
	case IOCTL_LCM_AUTO_SCROLL_OFF :
		spin_lock(&lcm_lock);
		lcm_auto_scroll_flag = 0;
		spin_unlock(&lcm_lock);
		break;
	case IOCTL_LCM_REVERSE_ON :
		spin_lock(&lcm_lock);
		lcm_reverse_flag = 1;
		spin_unlock(&lcm_lock);
		break;
	case IOCTL_LCM_REVERSE_OFF :
		spin_lock(&lcm_lock);
		lcm_reverse_flag = 0;
		spin_unlock(&lcm_lock);
		break;
	case IOCTL_LCM_SAVE_BYTE :
		if ( copy_from_user(&user_data, (void *)arg, sizeof(user_data)) )
			return -EFAULT;
		break;
	case IOCTL_LCM_WRITE_BYTE :
		{
		u16             addr;

		if ( copy_from_user(&pos, (void *)arg, sizeof(pos)) )
			return -EFAULT;
		if ( pos.x < 0 || pos.x >= LCM_MAX_X_DOTS || pos.y < 0 || pos.y >= LCM_MAX_Y_DOTS )
			return -EINVAL;
		spin_lock(&lcm_lock);
		lcm_pix_buffer[pos.y][pos.x/LCM_X_DOTS] = user_data;
		spin_unlock(&lcm_lock);
		addr = ((pos.y)*0x1e) + (pos.x/LCM_X_DOTS) ;
		LCM_two_arguments_cmd(LCM_CMD_SET_ADDR_POINT,(u16)(addr&0xFF),(u16)(addr>>8));
		LCM_one_argument_cmd(LCM_CMD_DW_INC,(u16)user_data);
		break;
		}
	case IOCTL_LCM_DISPLAY_OFF :
		LCM_no_argument_cmd(LCM_CMD_DISPLAY_OFF);
		break;
	case IOCTL_LCM_DISPLAY_ON :
		LCM_no_argument_cmd(LCM_CMD_TF_GO);
		break;
	default:
		return -EINVAL;
	}
	
	return 0;
}

static int lcm_open(struct inode *inode, struct file *file)
{
	if ( MINOR(inode->i_rdev) == MOXA_LCM_MINOR )
		return 0;
	return -ENODEV;
}

static int lcm_release(struct inode *inode, struct file *file)
{
	return 0;
}

static struct file_operations lcm_fops = {
	owner:THIS_MODULE,
	llseek:NULL,
	write:lcm_write,
	ioctl:lcm_ioctl,
	open:lcm_open,
	release:lcm_release,
};
static struct miscdevice lcm_dev = {
	MOXA_LCM_MINOR,
	"lcm",
	&lcm_fops
};

#if 1	// add by Victor Yu. 04-03-2007
extern int	lcm_module_flag;
#endif
static void __exit ivtc_lcm1_exit(void)
{
	misc_deregister(&lcm_dev);
	lcm_module_flag = 0;
}

static int __init ivtc_lcm1_init(void)
{

	printk("Moxa CPU misc: Register LCM module WG240128B misc ver1.0 ");
	if ( lcm_module_flag ) {
		printk("fail !\nThe other type LCM module device driver has loaded.\n");
		return -ENOMEM;
	}
	if ( misc_register(&lcm_dev) ) {
		printk("fail !\n");
		return -ENOMEM;
	}
	lcm_module_flag = 1;
	printk("OK.\n");

	/* text address & area
	jimmy_chen@moxa.com.tw:turn down text area, we use own font
	LCM_two_arguments_cmd(LCM_CMD_SET_T_H_ADDR,0,0) ;
	LCM_two_arguments_cmd(LCM_CMD_SET_T_AREA,T_AREA_SIZE,0) ;
	*/
	/* graphic address & area */
	LCM_two_arguments_cmd(LCM_CMD_SET_G_H_ADDR,0,0) ;
	LCM_two_arguments_cmd(LCM_CMD_SET_G_AREA,G_AREA_SIZE,0) ;
	/* set or mode */
	LCM_no_argument_cmd(LCM_CMD_OR_MODE) ;
	/* reset register off set is use by text area */
	LCM_two_arguments_cmd(LCM_CMD_SET_OFF_REG,0,0) ;
	/* set text off graphic on mode */
	LCM_no_argument_cmd(LCM_CMD_TF_GO) ;
	/* clear screen */
	LCM_clear() ;

	/*
	LCM_write_graph(0x00,fnt8x8[16]) ;
	LCM_write_graph(0x1e*1,fnt8x8[17]) ;
	LCM_write_graph(0x1e*2,fnt8x8[18]) ;
	LCM_write_graph(0x1e*3,fnt8x8[19]) ;
	LCM_write_graph(0x1e*4,fnt8x8[20]) ;
	LCM_write_graph(0x1e*5,fnt8x8[21]) ;
	LCM_write_graph(0x1e*6,fnt8x8[22]) ;
	LCM_write_graph(0x1e*7,fnt8x8[23]) ;
	*/

	return 0;
}

module_init(ivtc_lcm1_init);
module_exit(ivtc_lcm1_exit);

MODULE_AUTHOR("Victor Yu");
MODULE_LICENSE("GPL");
