/*
 * History:
 * Date		Aurhor			Comment
 * 02-23-2006	Victor Yu.		Create it.
 * 08-15-2006	Victor Yu. 		Modify to support UC-7110.
 * 02-09-2007	Victor Yu.		Porting to kernel version 2.6.19.
 */
//#define __KERNEL_SYSCALLS__
#if 1	// add by Victor Yu. 02-09-2007
#include <linux/version.h>
#endif
//#include <linux/config.h>
#include <linux/proc_fs.h>
//#include <linux/devfs_fs_kernel.h>
#include <linux/unistd.h>
#include <linux/string.h>
#include <linux/ctype.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/miscdevice.h>
#include <linux/fcntl.h>
#include <linux/init.h>
#include <linux/poll.h>
#include <linux/spinlock.h>
#include <linux/delay.h>
#include <linux/timer.h>
#include <linux/ioport.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/signal.h>
#include <linux/mm.h>
#include <linux/kmod.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include <asm/system.h>
#include <asm/irq.h>
#include <asm/bitops.h>
#include <asm/arch/moxa.h>

//#define VICTOR_DEBUG
#ifdef VICTOR_DEBUG
#define dbg_printk(x...)	printk(x)
#else
#define dbg_printk(x...)
#endif

#define MOXA_WATCHDOG_VERSION	"1.0"
#define MOXA_WATCHDOG_MINOR	WATCHDOG_MINOR
#define DEFAULT_WATCHDOG_TIME	(30UL*1000UL)	// 30 seconds
#define WATCHDOG_MIN_TIME	50UL		// 50 msec
#define WATCHDOG_MAX_TIME	(60UL*1000UL)	// 60 seconds
#define WATCHDOG_TOL_TIME	(500UL)		// 500 msec, for watchdog timer polling
#define WATCHDOG_TOL_COUNT_TIME	(1000UL)	// 1 seconds, for watchdog timer count
#define WATCHDOG_COUNTER(x)	((APB_CLK/1000UL)*(x))
#define WATCHDOG_JIFFIES(x)	((((x)+WATCHDOG_TOL_TIME)*HZ)/1000UL)

#if 0	// mask by Victor Yu. 05-15-2006
static spinlock_t		swtdlock=SPIN_LOCK_UNLOCKED;
#endif
static int			opencounts=0;
static int			swtduserenabled=0;
static unsigned long		swtdtime=DEFAULT_WATCHDOG_TIME;
static struct timer_list	swtdtimer;
static struct work_struct	rebootqueue;

static void swtd_enable(void)
{
	*(volatile unsigned int *)(CPE_WATCHDOG_BASE+4) = WATCHDOG_COUNTER(swtdtime+WATCHDOG_TOL_COUNT_TIME);
	*(volatile unsigned int *)(CPE_WATCHDOG_BASE+8) = 0x5ab9;
	*(volatile unsigned int *)(CPE_WATCHDOG_BASE+12) = 0x03;
}

static void swtd_disable(void)
{
	*(volatile unsigned int *)(CPE_WATCHDOG_BASE+12) = 0;
}

#define IOCTL_WATCHDOG_ENABLE		1	// enable watch dog and set time (unint msec)
#define IOCTL_WATCHDOG_DISABLE		2	// disable watch dog, kernle do it
#define IOCTL_WATCHDOG_GET_SETTING	3	// get now setting about mode and time
#define IOCTL_WATCHDOG_ACK		4	// to ack watch dog
struct swtd_set_struct {
	int		mode;
	unsigned long	time;
};
static int swtd_ioctl(struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg)
{
	unsigned long		time;
	struct swtd_set_struct	nowset;
	unsigned long		flags;

	switch ( cmd ) {
	case IOCTL_WATCHDOG_ENABLE :
		if ( copy_from_user(&time, (void *)arg, sizeof(time)) )
			return -EFAULT;
		if ( time < WATCHDOG_MIN_TIME || time > WATCHDOG_MAX_TIME )
			return -EINVAL;
#if 0	// mask by Victor Yu. 05-15-2006
		spin_lock(&swtdlock);
		del_timer(&swtdtimer);
		swtd_disable();
		swtdtime = time;
		swtduserenabled = 1;
		swtdtimer.expires = jiffies + WATCHDOG_JIFFIES(swtdtime);
		add_timer(&swtdtimer);
		swtd_enable();
		spin_unlock(&swtdlock);
#else	// add by Victor Yu. 05-15-2006
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,9)	// add by Victor Yu. 02-09-2007
		local_irq_save(flags);
#else
		save_flags(flags);
		cli();
#endif	// LINUX_VERSION_CODE
		if ( swtduserenabled ) {
			swtd_disable();
			del_timer(&swtdtimer);
		}
		swtdtime = time;
		swtduserenabled = 1;
		swtdtimer.expires = jiffies + WATCHDOG_JIFFIES(swtdtime);
		add_timer(&swtdtimer);
		swtd_enable();
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,9)	// add by Victor Yu. 02-09-2007
		local_irq_restore(flags);
#else
		restore_flags(flags);
#endif	// LINUX_VERSION_CODE
#endif
		break;
	case IOCTL_WATCHDOG_DISABLE :
#if 0	// mask by Victor Yu. 05-15-2006
		spin_lock(&swtdlock);
		if ( swtduserenabled ) {
			swtd_disable();
			del_timer(&swtdtimer);
			swtduserenabled = 0;
			swtdtime = DEFAULT_WATCHDOG_TIME;
			swtdtimer.expires = jiffies + WATCHDOG_JIFFIES(swtdtime);
			add_timer(&swtdtimer);
			swtd_enable();
		}
		spin_unlock(&swtdlock);
#else	// add by Victor Yu. 05-15-2006
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,9)	// add by Victor Yu. 02-09-2007
		local_irq_save(flags);
#else
		save_flags(flags);
		cli();
#endif	// LINUX_VERSION_CODE
		if ( swtduserenabled ) {
			swtd_disable();
			del_timer(&swtdtimer);
			swtduserenabled = 0;
		}
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,9)	// add by Victor Yu. 02-09-2007
		local_irq_restore(flags);
#else
		restore_flags(flags);
#endif	// LINUX_VERSION_CODE
#endif
		break;
	case IOCTL_WATCHDOG_GET_SETTING :
		nowset.mode = swtduserenabled;
		nowset.time = swtdtime;
		if ( copy_to_user((void *)arg, &nowset, sizeof(nowset)) )
			return -EFAULT;
		break;
	case IOCTL_WATCHDOG_ACK :
#if 0	// mask by Victor Yu. 05-15-2006
		spin_lock(&swtdlock);
		if ( swtduserenabled ) {
			swtd_disable();
			del_timer(&swtdtimer);
			swtdtimer.expires = jiffies + WATCHDOG_JIFFIES(swtdtime);
			add_timer(&swtdtimer);
			swtd_enable();
		}
		spin_unlock(&swtdlock);
#else	// add by Victor Yu. 05-15-2006
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,9)	// add by Victor Yu. 02-09-2007
		local_irq_save(flags);
#else
		save_flags(flags);
		cli();
#endif	// LINUX_VERSION_CODE
		if ( swtduserenabled ) {
			swtd_disable();
			del_timer(&swtdtimer);
			swtdtimer.expires = jiffies + WATCHDOG_JIFFIES(swtdtime);
			add_timer(&swtdtimer);
			swtd_enable();
		}
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,9)	// add by Victor Yu. 02-09-2007
		local_irq_restore(flags);
#else
		restore_flags(flags);
#endif	// LINUX_VERSION_CODE
#endif
		break;
	default:
		return -EINVAL;
	}
	return 0;
}

static int swtd_open(struct inode *inode, struct file *file)
{
	unsigned long	flags;

	if ( MINOR(inode->i_rdev) != MOXA_WATCHDOG_MINOR )
		return -ENODEV;
#if 0	// mask by Victor Yu. 05-15-2006
	spin_lock(&swtdlock);
	opencounts++;
	spin_unlock(&swtdlock);
#else	// add by Victor Yu. 05-15-2006
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,9)	// add by Victor Yu. 02-09-2007
	local_irq_save(flags);
#else
	save_flags(flags);
	cli();
#endif	// LINUX_VERSION_CODE
#if 0	// mask by Victor Yu. 05-15-2006
	if ( swtduserenabled == 0 ) {
		swtduserenabled = 1;
		swtdtimer.expires = jiffies + WATCHDOG_JIFFIES(swtdtime);
		add_timer(&swtdtimer);
		swtd_enable();
	}
#endif
	opencounts++;
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,9)	// add by Victor Yu. 02-09-2007
	local_irq_restore(flags);
#else
	restore_flags(flags);
#endif	// LINUX_VERSION_CODE
#endif
	return 0;
}

static int swtd_release(struct inode *inode, struct file *file)
{
	unsigned long	flags;

#if 0	// mask by Victor Yu. 05-15-2006
	spin_lock(&swtdlock);
#else	// add by Victor Yu. 05-15-2006
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,9)	// add by Victor Yu. 02-09-2007
	local_irq_save(flags);
#else
	save_flags(flags);
	cli();
#endif	// LINUX_VERSION_CODE
#endif
	dbg_printk("swtd_release entry.\n");
	opencounts--;
	if ( opencounts <= 0 ) {
#if 0	// mask by Victor Yu. 02-23-2006
		if ( signal_pending(current) ) {
			dbg_printk("swtd_release has signal pending\n");
			dbg_printk("parent signal blocked=0x%x,0x%x, pending=0x%x,0x%x\n", current->parent->blocked.sig[0], current->parent->blocked.sig[1], current->parent->pending.signal.sig[0], current->parent->pending.signal.sig[1]);
			dbg_printk("signal blocked=0x%x,0x%x, pending=0x%x,0x%x\n", current->blocked.sig[0], current->blocked.sig[1], current->pending.signal.sig[0], current->pending.signal.sig[1]);
			if ( sigismember(&current->parent->blocked, SIGKILL) ) {
				dbg_printk("swtd_release get SIGKILL signal\n");
				goto swtd_release_l1;
			}
			if ( sigismember(&current->blocked, SIGKILL) ) {
				dbg_printk("swtd_release get SIGKILL signal\n");
				goto swtd_release_l1;
			}
			if ( sigismember(&current->parent->blocked, SIGCHLD) ) {
				dbg_printk("swtd_release get SIGCHLD signal\n");
				goto swtd_release_l1;
			}
			if ( sigismember(&current->blocked, SIGCHLD) ) {
				dbg_printk("swtd_release get SIGCHLD signal\n");
				goto swtd_release_l1;
			}
			if ( sigismember(&current->parent->blocked, SIGTERM) ) {
				dbg_printk("swtd_release get SIGTERM signal\n");
				goto swtd_release_l1;
			}
			if ( sigismember(&current->blocked, SIGTERM) ) {
				dbg_printk("swtd_release get SIGTERM signal\n");
				goto swtd_release_l1;
			}
			if ( sigismember(&current->parent->blocked, SIGINT) ) {
				dbg_printk("swtd_release get SIGINT signal\n");
				goto swtd_release_l1;
			}
			if ( sigismember(&current->blocked, SIGINT) ) {
				dbg_printk("swtd_release get SIGINT signal\n");
				goto swtd_release_l1;
			}
#ifdef VICTOR_DEBUG	// add by Victor Yu. 02-23-2006
			if ( sigismember(&current->parent->blocked, SIGSTOP) ) {
				dbg_printk("swtd_release get SIGSTOP signal\n");
			}
			if ( sigismember(&current->parent->blocked, SIGHUP) ) {
				dbg_printk("swtd_release get SIGHUP signal\n");
			}
			if ( sigismember(&current->parent->blocked, SIGQUIT) ) {
				dbg_printk("swtd_release get SIGQUIT signal\n");
			}
			if ( sigismember(&current->parent->blocked, SIGTSTP) ) {
				dbg_printk("swtd_release get SIGTSTP signal\n");
			}
			if ( sigismember(&current->parent->blocked, SIGABRT) ) {
				dbg_printk("swtd_release get SIGABRT signal\n");
			}
			if ( sigismember(&current->parent->blocked, SIGSEGV) ) {
				dbg_printk("swtd_release get SIGSEGV signal\n");
			}
#endif
		} else {	// normal close the file handle
swtd_release_l1:
#endif
			if ( swtduserenabled ) {
				swtd_disable();
				del_timer(&swtdtimer);
				swtduserenabled = 0;
#if 0	// mask by Victor Yu. 05-15-2006
				swtdtime = DEFAULT_WATCHDOG_TIME;
				swtdtimer.expires = jiffies + WATCHDOG_JIFFIES(swtdtime);
				add_timer(&swtdtimer);
				swtd_enable();
#endif
			}
#if 0	// mask by Victor Yu. 02-23-2006
		}
#endif
		opencounts = 0;
	}
#if 0	// mask by Victor Yu. 05-15-1006
	spin_unlock(&swtdlock);
#else	// add by Victor Yu. 05-15-2006
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,9)	// add by Victor Yu. 02-09-2007
	local_irq_restore(flags);
#else
	restore_flags(flags);
#endif	// LINUX_VERSION_CODE
#endif
	return 0;
}

static void swtd_reboot(void *unused)
{
	char    *argv[2], *envp[5];

        if ( in_interrupt() )
                return;
        if ( !current->fs->root )
                return;
        argv[0] = "/bin/reboot";
        argv[1] = 0;
        envp[0] = "HOME=/";
        envp[1] = "PATH=/sbin:/bin:/usr/sbin:/usr/bin";
        envp[2] = 0;
        call_usermodehelper(argv[0], argv, envp, 0);
}

static void swtd_poll(unsigned long ignore)
{
	unsigned long	flags;

#if 0	// mask by Victor Yu. 05-15-2006
	spin_lock(&swtdlock);
#else	// add by Victor Yu. 05-15-2006
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,9)	// add by Victor Yu. 02-09-2007
	local_irq_save(flags);
#else
	save_flags(flags);
	cli();
#endif	// LINUX_VERSION_CODE
#endif
	swtd_disable();
	del_timer(&swtdtimer);
#if 0	// mask by Victor Yu. 05-15-2006
	if ( swtduserenabled ) {
		dbg_printk("Now reboot the system.\n");
		schedule_work(&rebootqueue);
	} else {
		swtdtimer.expires = jiffies + WATCHDOG_JIFFIES(swtdtime);
		add_timer(&swtdtimer);
		swtd_enable();
	}
	spin_unlock(&swtdlock);
#else	// add by Victor Yu. 05-15-2006
	dbg_printk("Now reboot the system.\n");
	schedule_work(&rebootqueue);
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,9)	// add by Victor Yu. 02-09-2007
	local_irq_restore(flags);
#else
	restore_flags(flags);
#endif	// LINUX_VERSION_CODE
#endif
}

static int swtd_proc_output(char *buf)
{
	char	*p;

	p = buf;
	p += sprintf(p,
		     "user enable\t: %d\n"
		     "ack time\t: %d msec\n",
		     swtduserenabled, (int)swtdtime);
	return p - buf;
}

static int swtd_read_proc(char *page, char **start, off_t off, int count, int *eof, void *data)
{
	int	len=swtd_proc_output(page);

	if ( len <= off + count )
		*eof = 1;
	*start = page + off;
	len -= off;
	if ( len > count )
		len = count;
	if ( len < 0 )
		len = 0;
	return len;
}

static struct file_operations swtd_fops = {
	owner:THIS_MODULE,
	llseek:NULL,
	ioctl:swtd_ioctl,
	open:swtd_open,
	release:swtd_release,
};
static struct miscdevice swtd_dev = {
	MOXA_WATCHDOG_MINOR,
	"swtd",
	&swtd_fops
};

static void __exit moxa_swtd_exit(void)
{
	unsigned long	flags;

#if 0	// mask by Victor Yu. 05-15-2006
	spin_lock(&swtdlock);
#else	// add by Victor Yu. 05-15-2006
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,9)	// add by Victor Yu. 02-09-2007
	local_irq_save(flags);
#else
	save_flags(flags);
	cli();
#endif	// LINUX_VERSION_CODE
#endif
	if ( swtduserenabled ) {
		swtd_disable();
		del_timer(&swtdtimer);
		swtduserenabled = 0;
		opencounts = 0;
	}
#if 0	// mask by Victor Yu. 05-15-2006
	spin_unlock(&swtdlock);
#else	// add by Victor Yu. 05-15-2006
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,9)	// add by Victor Yu. 02-09-2007
	local_irq_restore(flags);
#else
	restore_flags(flags);
#endif	// LINUX_VERSION_CODE
#endif
	misc_deregister(&swtd_dev);
}

static int __init moxa_swtd_init(void)
{
	// register misc
	if ( misc_register(&swtd_dev) ) {
		printk("Moxa ART CPU WatchDog: Register misc fail !\n");
		goto moxa_swtd_init_err;
	}
	INIT_WORK(&rebootqueue, swtd_reboot, NULL);
#if 0	// mask by Victor Yu. 05-15-2006
	spin_lock(&swtdlock);
#endif
	init_timer(&swtdtimer);
	swtdtimer.function = swtd_poll;
#if 0	// mask by Victor Yu. 05-15-2006
	swtdtimer.expires = jiffies + WATCHDOG_JIFFIES(swtdtime);
	add_timer(&swtdtimer);
	swtd_enable();
	spin_unlock(&swtdlock);
#endif
	create_proc_read_entry("driver/swtd", 0, 0, swtd_read_proc, NULL);
	printk("Moxa ART CPU WatchDog driver v" MOXA_WATCHDOG_VERSION "\n");

	return 0;

moxa_swtd_init_err:
	misc_deregister(&swtd_dev);
	return -ENOMEM;
}

module_init(moxa_swtd_init);
module_exit(moxa_swtd_exit);

MODULE_AUTHOR("Victor Yu");
MODULE_LICENSE("GPL");
