
/* 
 * This is a include file to define Moxa supporting LCM moudle global defined.
 *
 * History:
 * Date		Author			Commnet
 * 09-03-2008	Victor Yu.		Create it.
 */

#ifndef __MOXA_LCM_H
#define __MOXA_LCM_H

// ioctl command define
#define IOCTL_LCM_GOTO_XY		1
#define IOCTL_LCM_CLS			2
#define IOCTL_LCM_CLEAN_LINE		3
#define IOCTL_LCM_GET_XY		4
#define IOCTL_LCM_BACK_LIGHT_ON		5
#define IOCTL_LCM_BACK_LIGHT_OFF	6
#define IOCTL_LCM_PIX_ON		7
#define IOCTL_LCM_PIX_OFF		8
#define IOCTL_LCM_AUTO_SCROLL_ON	13
#define IOCTL_LCM_AUTO_SCROLL_OFF	14
#define IOCTL_LCM_SAVE_BYTE		15
#define IOCTL_LCM_WRITE_BYTE		16
#define IOCTL_LCM_DISPLAY_OFF		17
#define IOCTL_LCM_DISPLAY_ON		18
#define IOCTL_LCM_REVERSE_ON		20
#define IOCTL_LCM_REVERSE_OFF		21

#endif	// __MOXA_LCM_H
