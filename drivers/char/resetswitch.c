/***************************************************************************/

/*
 *	linux/drivers/char/resetswitch.c
 *
 *	Basic driver to support the Moxart software reset button.
 *
 * 	Jimmy_chen@moxa.com.tw
 */

/***************************************************************************/

#if 1	// add by Victor Yu. 02-09-2007
#include <linux/version.h>
#endif
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,12)	// add by Victor Yu. 02-09-2007
#include <linux/config.h>
#endif
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/param.h>
#include <linux/init.h>
#include <asm/irq.h>
#include <asm/traps.h>
#include <linux/delay.h>
#include <linux/sched.h>
#include <linux/interrupt.h>
#include <linux/timer.h>
#include <linux/syscalls.h>
#include <linux/reboot.h>
#include <asm/uaccess.h>
#include <asm/io.h>
#include <asm/arch/moxa.h>
#include <asm/arch/gpio.h>

#define RESET_POLL_TIME			(HZ/5)
#define RESET_TIMEOUT			(HZ * 5)

#define PIO(x)				1<<x

#if defined(CONFIG_ARCH_W311_TEST)	// add by VIctor Yu. 07-05-2008
#define RESET_IO 			PIO(23)
#define LED_READY			PIO(4)
#else
#define RESET_IO 			PIO(25)
#define LED_READY			PIO(27)
#endif

struct work_struct		resetqueue;
static struct timer_list	resettimer;
static spinlock_t		resetlock=SPIN_LOCK_UNLOCKED;
static unsigned long		endresettime, intervaltime;
static int			ledonoffflag;

static void reset_to_default(void)
{
	char *argv[3] , *envp[4] ;

	envp[0] = "HOME=/" ;
	envp[1] = "PATH=/sbin:/bin:/usr/sbin:/usr/bin" ;
	envp[2] = "TERM=console" ;
	envp[3] = NULL ;

	argv[0] = "ldfactory" ;
	argv[1] = NULL ;

	printk("reset to default\n") ;
       	if(call_usermodehelper("/bin/ldfactory",argv,envp,0)==-1)
		printk("set to default failure\n") ;
}

static void reset_poll(unsigned long ingore)
{
	spin_lock(&resetlock);
	del_timer(&resettimer);
	if ( !mcpu_gpio_get(RESET_IO) ) {
		if ( endresettime == 0 ) {
			endresettime = jiffies + RESET_TIMEOUT;
			intervaltime = jiffies + HZ;
		} else if ( time_after(jiffies, endresettime) ){
			mcpu_gpio_set(LED_READY, MCPU_GPIO_HIGH);
			schedule_work(&resetqueue);
			goto poll_exit;
		} else if ( time_after(jiffies, intervaltime) ) {
			if ( ledonoffflag ) {
				ledonoffflag = 0;
				mcpu_gpio_set(LED_READY, MCPU_GPIO_HIGH);
			} else {
				ledonoffflag = 1;
				mcpu_gpio_set(LED_READY, MCPU_GPIO_LOW);
			}
			intervaltime = jiffies + HZ;
		}
	} else if ( endresettime ) {
		endresettime = 0;
		ledonoffflag = 1;
		mcpu_gpio_set(LED_READY, MCPU_GPIO_LOW);
	}
	resettimer.function = reset_poll;
	resettimer.expires = jiffies + RESET_POLL_TIME;
	add_timer(&resettimer);
poll_exit:
	spin_unlock(&resetlock);
}

/***************************************************************************/
static void __exit resetswitch_exit(void)
{
	spin_lock(&resetlock);
	del_timer(&resettimer);
	spin_unlock(&resetlock);
}

static int resetswitch_init(void)
{
	// initialize the GPIO mode and directly
	mcpu_gpio_mp_set(RESET_IO|LED_READY);
	mcpu_gpio_inout(RESET_IO, MCPU_GPIO_INPUT);
	mcpu_gpio_inout(LED_READY, MCPU_GPIO_OUTPUT);

	INIT_WORK(&resetqueue, reset_to_default, NULL);
	spin_lock(&resetlock);
	endresettime = 0;
	ledonoffflag = 1;
	init_timer(&resettimer);
	resettimer.function = reset_poll;
	resettimer.expires = jiffies + RESET_POLL_TIME;
	add_timer(&resettimer);
	spin_unlock(&resetlock);

	return 0;
}

module_init(resetswitch_init);
module_exit(resetswitch_exit);

/***************************************************************************/
